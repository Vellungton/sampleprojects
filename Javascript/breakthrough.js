// Tämä peli on tehty Web-sovellukset kurssilleni yhtenä harjoitustyönä. Idea tehtävässä on tehdä koko sisältö
// JavaScriptin kautta. Tehtävänannossa annettiin pelin säännöt, ja toteutus oli tekijästä kiinni.
// Tykkäsin itse tuosta pelin ideasta, joten tein siitä ihan toimivan mallin. Skaalautuvuus vielä vähän kökköä.

var rivit;
var height;
var width;
var otsikko;
var style1;
var html;
var body;
var ruutu;
var leveys;

window.onload = function() {
    body = document.body;
    var head = document.getElementsByTagName('head')[0];
    
    html = document.documentElement;
    height = Math.max( body.scrollHeight, body.offsetHeight,
                        html.clientHeight, html.scrollHeight, html.offsetHeight ); 
    //height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
    width = Math.max( body.offsetWidth , body.scrollWidth, html.clientWidth, html.clientWidth, html.offsetWidth );
    var pienempi = Math.min(height, width);
    otsikko = width*0.5;
    
    style1=document.createElement('style');
    style1.setAttribute('type','text/css');
    head.appendChild(style1);
    
    ruutu = height/(32*1.6);//width/12;
    
    
    addstyle(style1, '.musta {background-color: black;color: white; }');
    addstyle(style1, 'span.blue {background: radial-gradient(ellipse at center, #f0f9ff 0%, #cbebff 47%, #a1dbff 100%);}');
    addstyle(style1, 'span.red {background: radial-gradient(ellipse at center, #fff9f0 0%, #ffebcb 47%, #ffdba1 100%); }');
    addstyle(style1, 'span.selected {background: radial-gradient(ellipse at center, #D76B56 0%, #cbebff 47%, #ffdba1 100%); }');
    addstyle(style1, 'span.voitto {background: radial-gradient(ellipse at center, #87C540 0%, #4285F4 47%, #9C584D 100%); }');
    addstyle(style1, 'h1 {width: ' + otsikko +'px; background-color: blue;}');

    addstyle(style1, 'td {min-width: '+ruutu+'px; min-height: '+(ruutu)+'px; width: '+ruutu*1.4+'px; height: '+(ruutu)+'px; border: 1px solid black; margin: 0; padding: 0;}');
    addstyle(style1, 'table {height: '+height+'px; border-spacing: 0; border-collapse: collapse;}');
    addstyle(style1, 'span {margin: '+ruutu*0.01+'px; padding: 0; min-width: '+ruutu+'px; min-height: '+ruutu*1.35+'px; display: block; border-radius: 50%;}');
    //addstyle(style1, 'span {margin: 5px; padding: 0; min-width: '+ruutu+'px; min-height: '+ruutu+'px; display: block; border-radius: 50%;}');
    //addstyle(style1, 'table {height: '+height+'px; border-spacing: 0; border-collapse: collapse;}');
    
    var p1=document.createElement('p');
    var txt1=document.createTextNode('Kerro luotavan ruudukon koko. Ruudukko on yhtä monta ruutua\nleveä kuin korkea. ');
    p1.appendChild(txt1);
    body.appendChild(p1);
    var form1=document.createElement('form');
    form1.setAttribute('id','ruudukko');
    var fieldset1=document.createElement('fieldset');
    form1.appendChild(fieldset1);
    var p1=document.createElement('p');
    fieldset1.appendChild(p1);
    var label1=document.createElement('label');
    p1.appendChild(label1);
    var txt3=document.createTextNode('Leveys ');
    label1.appendChild(txt3);
    var input1=ce('input','x');
    input1.setAttribute('type','text');
    input1.setAttribute('id', 'leveys');
    label1.appendChild(input1);
    var p2=document.createElement('p');
    form1.appendChild(p2);
    var input2=document.createElement('input');
    input2.setAttribute('type','submit');
    input2.setAttribute('value','Luo');
    input2.addEventListener('click', stopDefAction, false);
    p2.appendChild(input2);
    body.appendChild(form1);
    
    var h11=document.createElement('h1');
    h11.setAttribute('id', 'h1taulukko');
    var txt1=document.createTextNode('Ruudukko');
    h11.appendChild(txt1);
    body.appendChild(h11);
    
    var table=document.createElement('table');
    table.setAttribute('id', 'taulukko');
    var tbody1=document.createElement('tbody');
    table.appendChild(tbody1);
    body.appendChild(table);
    
}

var ekavalittu = false;
var mikavalittu;
var punaisenvuoro = true;
var voitto = false;
var paalla = true;

window.onresize = taulukonkoko;

function stopDefAction(evt) {
    paalla = true;
    voitto = false;
    evt.preventDefault();
    var taulukko = document.getElementById('taulukko');
    leveys = parseInt(document.getElementById('leveys').value, 10);//var leveys = parseInt(document.getElementById('leveys').value, 10);
    if (leveys<8 || leveys>32){
     return;   
    }
    rivit = leveys;
    luo_haluttu(leveys, taulukko);
    taulukonkoko();
}

function taulukonkoko(){
    var poistettava = style1.lastChild;
    style1.removeChild(poistettava);
    poistettava = style1.lastChild;
    style1.removeChild(poistettava);
    poistettava = style1.lastChild;
    style1.removeChild(poistettava);
    height = Math.max( body.scrollHeight, body.offsetHeight,
                        html.clientHeight, html.scrollHeight, html.offsetHeight );
    ruutu = height/(rivit*1.6);
    addstyle(style1, 'td {min-width: '+ruutu+'px; min-height: '+(ruutu)+'px; width: '+ruutu*1.4+'px; height: '+(ruutu*1.2)+'px; border: 1px solid black; margin: 0; padding: 0;}');
    addstyle(style1, 'table {height: '+height+'px; border-spacing: 0; border-collapse: collapse;}');
    addstyle(style1, 'span {margin: '+ruutu*0.01+'px; padding: 0; min-width: '+ruutu+'px; min-height: '+ruutu*1.15+'px; display: block; border-radius: 50%;}');
}



function valittu(event){
    if (paalla){
    var valittu = event.target;

    var mikaon = valittu.getAttribute('class');
    if (ekavalittu){
        siirra(valittu);   
    }
    else {
        if ((mikaon == 'red') && punaisenvuoro){
            valittu.className='selected';
            ekavalittu = true;
            mikavalittu = valittu;
        }
        if ((mikaon == 'blue') && (punaisenvuoro == false)){
            valittu.className='selected';
            ekavalittu = true;
            mikavalittu = valittu;
        }
    }
}
}

function siirra(tokavalinta){
    if (punaisenvuoro && (tokavalinta.getAttribute('class') == 'red')){
        mikavalittu.className='red';
        mikavalittu = tokavalinta;
        tokavalinta.className='selected';
        return;
    }
    else if (!punaisenvuoro && (tokavalinta.getAttribute('class') == 'blue')){
        mikavalittu.className='blue';
        mikavalittu = tokavalinta;
        tokavalinta.className='selected';
        return;
    } 
    else if(punaisenvuoro && (tokavalinta.getAttribute('class') == 'blue') && tarkistasiirto(tokavalinta)){
            var apurieka = mikavalittu.parentElement;
            apurieka.removeChild(apurieka.firstChild);
            var apuritoka = tokavalinta.parentElement;
            apuritoka.removeChild(apuritoka.firstChild);
            var span = document.createElement('span');
            if (voitto){
                span.className='voitto';   
            } else {
               span.className='red'; 
            }
            apuritoka.appendChild(span);
            punaisenvuoro = false;
    }
    else if(!punaisenvuoro && ((tokavalinta.getAttribute('class') == 'red')) && tarkistasiirto(tokavalinta)){
            var apurieka = mikavalittu.parentElement;
            apurieka.removeChild(apurieka.firstChild);
            var apuritoka = tokavalinta.parentElement;
            apuritoka.removeChild(apuritoka.firstChild);
            var span = document.createElement('span');
            if (voitto){
                span.className='voitto';   
            } else {
               span.className='blue'; 
            }
            apuritoka.appendChild(span);
            punaisenvuoro = true;
    }
    else {
        if(punaisenvuoro && tokavalinta.hasAttribute('id') && tarkistasiirto(tokavalinta)){ // && tokavalinta.hasChildNodes
            var apurieka = mikavalittu.parentElement;
            apurieka.removeChild(apurieka.firstChild);
            var span = document.createElement('span');
            if (voitto){
                span.className='voitto';   
            } else {
               span.className='red'; 
            }
            if (tokavalinta.hasChildNodes()){
            tokavalinta.replaceChild(span, tokavalinta.firstChild);
            } else {
            tokavalinta.appendChild(span); 
            }
            punaisenvuoro = false;
        }
        if(!punaisenvuoro && tokavalinta.hasAttribute('id') && tarkistasiirto(tokavalinta)){//hasAttribute('id')
            var apurieka = mikavalittu.parentElement;
            apurieka.removeChild(apurieka.firstChild);
            var span = document.createElement('span');
            if (voitto){
                span.className='voitto';   
            } else {
               span.className='blue'; 
            }
            if (tokavalinta.hasChildNodes()){
            tokavalinta.replaceChild(span, tokavalinta.firstChild);
            } else {
            tokavalinta.appendChild(span); 
            }
            punaisenvuoro = true;
        }
    }
    
    
    }

function tarkistasiirto(kayko){
    var toka;
    //var eka = parseInt(mikavalittu.parentElement.getAttribute('id'),10);
    var ekajono = mikavalittu.parentElement.getAttribute('id');
    var eka = parseInt(ekajono.substr(1), 10);
    
    
    var vastustaja = false;
    
    if (kayko.hasAttribute('id') && kayko.hasChildNodes()){
        var lapsi = kayko.firstChild;
        if ((lapsi.getAttribute('class') == 'red') && punaisenvuoro){
            return false;
        }
        if ((lapsi.getAttribute('class') == 'blue') && !punaisenvuoro){
            return false;
        }
        //toka = parseInt(kayko.getAttribute('id'), 10);
        toka = parseInt(kayko.getAttribute('id').substr(1), 10);
        vastustaja = true;
    }
    
    if (kayko.hasAttribute('id')){
        //toka = parseInt(kayko.getAttribute('id'), 10);
        toka = parseInt(kayko.getAttribute('id').substr(1), 10);
    } else {
        toka = parseInt(kayko.parentElement.getAttribute('id').substr(1), 10);
        vastustaja = true;
    }
    
    
    if (punaisenvuoro && vastustaja && (toka == eka+rivit)){
        vastustaja = false;
        return false;
    }
    if (punaisenvuoro && (eka%rivit==0)){
        if((toka == eka+rivit) || (toka == eka+(rivit+1))){
            if (toka > ((rivit*rivit)-(rivit+1))){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    } else if (punaisenvuoro && (eka%rivit==(rivit-1))){
        if((toka == eka+(rivit-1)) || (toka == eka+(rivit))){
            if (toka > ((rivit*rivit)-(rivit+1))){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    } else if (punaisenvuoro && ((eka%rivit)>0 && (eka%rivit)<(rivit-1))){
        if((toka == eka+(rivit-1)) || (toka == eka+rivit) || (toka == eka+(rivit+1))){
            if (toka > ((rivit*rivit)-(rivit+1))){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    }
    if (!punaisenvuoro && vastustaja && (toka == eka-rivit)){
        vastustaja = false;
        return false;
    }
    if (!punaisenvuoro && (eka%rivit==0)){
        if((toka == eka-rivit) || (toka == eka-(rivit-1))){
            if (toka < rivit){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    } else if (!punaisenvuoro && (eka%rivit==(rivit-1))){
        if((toka == eka-rivit) || (toka == eka-(rivit+1))){
            if (toka < rivit){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    } else if (!punaisenvuoro && ((eka%rivit)>0 && (eka%rivit)<(rivit-1))){
        if((toka == eka-(rivit-1)) || (toka == eka-rivit) || (toka == eka-(rivit+1))){
            if (toka < rivit){
                paalla=false;
                voitto = true;
            }
            return true;
        }
    }
        
    return false;
}

function luo_haluttu(leveys, table){
    var body =document.body;
    var table1=document.createElement('table');
    table1.setAttribute('id', 'taulukko');
    var tbody1=document.createElement('tbody');
    table1.appendChild(tbody1);
    for(var i=0;i<leveys;i++){
        var tr=document.createElement('tr');
        tbody1.appendChild(tr);
        for (var j=0;j<leveys;j++){
            var td=document.createElement('td');
            
            var luku = 'i' + ((i*leveys)+j);
            
            //td.setAttribute('id','i'+luku);
            td.setAttribute('id',luku);
            
            td.addEventListener('click', valittu, false);
            if((i+j)%2==0){
                td.className='musta';
            } else {
                td.className='valkoinen';   
            }
            if (i<2){
                var span = document.createElement('span');
                span.className='red';
                td.appendChild(span);   
            }
            if (i>(leveys-3)){
                var span = document.createElement('span');
                span.className='blue';
                td.appendChild(span);
                
            }
            tr.appendChild(td);
        }
    }
    body.replaceChild(table1, table);
}

function ce(tag,name){
      var element;
      if (name && window.ActiveXObject){
        element = document.createElement('<'+tag+' name="'+name+'">');
      }else{
        element = document.createElement(tag);
        element.setAttribute('name',name);
      }
      return element;
}

function addstyle(style1, tyylitieto){
    var txt1=document.createTextNode(tyylitieto);
    style1.appendChild(txt1);
}
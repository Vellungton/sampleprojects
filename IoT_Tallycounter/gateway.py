# -*- coding: utf-8 -*-

import requests
import Adafruit_BluefruitLE
from Adafruit_BluefruitLE.services import UART
import time
import math
import json

s = requests.session()
# Get the BLE provider for the current platform.
ble = Adafruit_BluefruitLE.get_provider()

def sendButtonToServer(button):
    # REST-tyyppisesti haetaan device-sivua "painettu nappia 4"
    url = 'http://.. secret ..laskuri.cgi/device'
    #url = 'http://.. secret ..amazonaws.com/device'
    data = {"luku": str(button)}
    r = s.get(url, params=data)
    print (r.url)
    print r.text
    return str(r.text)

def loginToServer(name, value):
    # Kirjaudutaan ensin sisaan
    url = 'http://.. secret ..laskuri.cgi/kirjaudu'
    #url = 'http://.. secret ..amazonaws.com/kirjaudu'
    data = {name: str(value)}
    r = s.post(url, data)
    print r.text
    return str(r.text)

def logoutFromServer():
    # Lopetetaan sessio, eli logout
    url = 'http://.. secret ..laskuri.cgi/logout'
    #url = 'http://.. secret ..amazonaws.com/logout'
    r = s.post(url)
    print r.text

def main():
    # Clear any cached data
    ble.clear_cached_data()

    # Initialize the adapter - I'm on MAC that has BLE
    adapter = ble.get_default_adapter()
    adapter.power_on()
    print('Using adapter: {0}'.format(adapter.name))

    # Disconnect any previously connected devices
    print('Disconnecting any connected UART devices...')
    UART.disconnect_devices()

    # Scan for UART devices.
    # Connect to first device if found
    print('Searching for UART device...')
    try:
        adapter.start_scan()
        device = UART.find_device()
        if device is None:
            raise RuntimeError('Failed to find UART device!')
    finally:
        adapter.stop_scan()
    print('Connecting to device...')
    device.connect()

    # Try to use UART-BLE channel
    try:
        # Wait for 60 sec to discover possible devices
        print('Discovering services...')
        UART.discover(device)
        uart = UART(device)

        # Bluefruit BLE-buffer can handle only 20 characters!
        uart.write('Server con ok')

        # Wait for message from device for 20 sec
        print('Waiting up to 20 seconds to receive data from the device...')
        loop = 0

        # First wait 20 sec to receive data from device
        # Then pass the login data to the Flask
        # If received something, then move on to data passing phase
        # Its up to Flask to decide if login keyid is good
        received = uart.read(timeout_sec=20)
        if received is not None:
            # Received data, print it out, and pass it on to Flask
            print('Received: {0}'.format(received))
            loop = 1
            try:
                viesti = loginToServer('keyid', '{0}'.format(received))
                viestia = json.loads(viesti)
                bufferRounds = len(viesti) // 20 + 1
                bufferStart = 0 # The Bluefruit BLE-shield has only 20 bytes of UART-buffer
                bufferLimit = 20 # so one needs to go around this somehow
                    
                for lap in range(0,bufferRounds):
                    print(viesti[bufferStart:bufferLimit])
                    uart.write(viesti[bufferStart:bufferLimit])
                    bufferStart += 20
                    bufferLimit += 20
                    time.sleep(0.05)

            except:
                uart.write("ei tullut login")
                print("Error in sending login data")
        else:
            # Timeout waiting for data, None is returned.
            uart.write('BLE shutdown.')
            uart.write('Timeout read.')
            print('Received no data from login!')

        # This loop is only entered if login receives anything
        # Push all incoming data to Flask
        # Its Flasks job to handel the data and decide what to do
        # Exit this loop only, if error or timeout occures
        while (loop != 0):
            received = uart.read(timeout_sec=10)
            if received is not None:
                # Print received data
                print('Received: {0}'.format(received))
                try:
                    # Push data to Flask and then pass the response to device
                    response = sendButtonToServer('{0}'.format(received))
                    # Load json although it is pushed as a string to the device, just to see it works :)
                    responses = json.loads(response)

                    bufferRounds = len(response) // 20 + 1
                    bufferStart = 0 # The Bluefruit BLE-shield has only 20 bytes of UART-buffer
                    bufferLimit = 20 # so one needs to go around this somehow
                    
                    for lap in range(0,bufferRounds):
                        print(response[bufferStart:bufferLimit])
                        uart.write(response[bufferStart:bufferLimit])

                        bufferStart += 20
                        bufferLimit += 20
                        time.sleep(0.05)
                        
                except:
                    print("Error in sending button data")
                    uart.write("Error sendin data")
                    loop = 0
            else:
                # Timeout waiting for data, None is returned.
                uart.write('BLE shutdown.   ')
                uart.write('Timeout button.')
                print('Received no data from button!')
                loop = 0
                logoutFromServer()
                

    finally:
        # Make sure device is disconnected on exit, and uart gets to write everything.
        # Sleep is just for securing uart gets fully written
        time.sleep(2)
        device.disconnect()


# Initialize the BLE system.  MUST be called before other BLE calls!
ble.initialize()

# Start the mainloop to process BLE events, and run the provided function in
# a background thread.  When the provided main function stops running, returns
# an integer status code, or throws an error the program will exit.
ble.run_mainloop_with(main)

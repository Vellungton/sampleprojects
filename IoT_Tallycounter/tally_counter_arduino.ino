#include <Arduino.h>
#include <SPI.h>
#include <LiquidCrystal.h>
#include <ArduinoJson.h>


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(10, 9, 6, 5, 3, 2);

// These pins are for the buttons
const int button0 = 16;
const int button1 = 15;
const int button2 = 17;
const int button3 = 14;
volatile int state0 = 0;
volatile int state1 = 0;
volatile int state2 = 0;
volatile int state3 = 0;

// "testi1" and "testi2" are valid keyids
String keyid = "testi2";

// For parsing the message
String message = "";

// These are ready libraries for the BLE-shield
#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "BluefruitConfig.h"

    #define FACTORYRESET_ENABLE         1
    #define MINIMUM_FIRMWARE_VERSION    "0.6.6"
    #define MODE_LED_BEHAVIOUR          "MODE"

/* ...hardware SPI, using SCK/MOSI/MISO hardware SPI pins and then user selected CS/IRQ/RST */
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}



// Prints the message to the 16x2 LCD and empties it.
void printAndEmpty(String viesti) {
  Serial.println(viesti);
  
  lcd.clear();
  lcd.setCursor(0, 0);
  
  if (viesti.length() <= 16){
    lcd.print(viesti);
  }
  else if (viesti.length() <= 32) {
    lcd.print(viesti.substring(0,16));
    lcd.setCursor(0, 1);
    lcd.print(viesti.substring(16));
  }
  else {
    lcd.print(viesti.substring(0,16));
    lcd.setCursor(0, 1);
    lcd.print(viesti.substring(16,33));
  }

  message = "";
}

// Checks if the String begins with {
// thus delivering it to either printJson or printAndEmpty
void messageFormat(String viesti){
  if(viesti.charAt(0) != '{'){
    printAndEmpty(viesti);
    Serial.println("char at ei ole");
    return;
  }
  else{
    printJson(viesti);
  }
}

// Parses the received JSON-format string into JSON-object,
// and prints the message to the 16x2 LCD screen
void printJson(String viesti) {
  Serial.println(viesti);

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(viesti); 

  String values[] = {root["b0"], root["b1"], root["b2"], root["b3"]};
  
  lcd.clear();
  lcd.setCursor(0, 0);

  for(int i = 0; i < 4; i++){
    if(i == 2){
      lcd.setCursor(0, 1);
    }
    
    if(i == 0 || i == 2){
      String newString = values[i].substring(0,7);
      for(int j = 0; j < (9 - values[i].length()); j++){
        newString.concat(" ");
      }
      lcd.print(newString);
    }
    else{
      lcd.print(values[i].substring(0,7));
    }
  }
  message = "";
}

void verifyKeyID(){
  ble.print(keyid);
  delay(100);
}

/**************************************************************************/
/*!
 *  This is a setup(), that gets run only once every time the Arduino is powered on:
    - initialize the LCD-display
    - initialize the buttons for input
    - factory reset the BLE-shield
    - wait until connections established
    - set the LED to blue color if connection OK
    - send login data and keyid to web-server
    
*/
/**************************************************************************/

void setup(void)
{
  // Init the display
  lcd.begin(16, 2);
  lcd.clear();
  
  // Init the buttons
  pinMode(button0, INPUT);
  digitalWrite(button0, LOW);
  pinMode(button1, INPUT);
  digitalWrite(button1, LOW);
  pinMode(button2, INPUT);
  digitalWrite(button2, LOW);
  pinMode(button3, INPUT);
  digitalWrite(button3, LOW);
  
  // Serial is here for debugging purposes only, will be removed
  Serial.begin(115200);

  printAndEmpty("Waiting for BLE connection");

  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  ble.verbose(false);  // debug info is a little annoying after this point!

  /* Wait for connection */
  while (! ble.isConnected()) {
      delay(500);
  }

  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
  }

  ble.setMode(BLUEFRUIT_MODE_DATA);

  printAndEmpty("BLE ok. Waiting for server");

  // Stay in this loop for as long as receive something from BLE-buffer
  // After received, post keyid and move on to the loop
  boolean login = false;
  while ( login == false )
  {
    while ( ble.available() ){
    int c = ble.read();
    message += (char)c;
    }

    if (message.length() > 0){
      Serial.println("Tämä on setupista:");
      Serial.println(message);
      printAndEmpty(message);
      delay(1000); // This is simply for letting UART-channel to initialize
      verifyKeyID();
      login = true;
    }
  }
  
}

/**************************************************************************/
/*!
    Constantly poll for pushed buttons or response data from BLE-buffer
*/
/**************************************************************************/
void loop(void)
{
  // If there is chars in BLE-buffer, then parse them into a String
  while ( ble.available() ){
    // After reading the first buffer entry, wait for a 300 ms and read it again.
    // This is do to the buffer size of 20 bytes on the BLE python code in the gateway
    while ( ble.available() ){
    int c = ble.read();
    message += (char)c;
    }
    delay(500);
  }

  // If there is something received, then print it!
  if (message.length() > 0){
    // printAndEmpty(message);
    Serial.println("tämä tuli loopista:");
    Serial.println(message);
    messageFormat(message);
  }

  // For every round, read the button state - if pressed --> state = high
  state0 = digitalRead(button0);
  state1 = digitalRead(button1);
  state2 = digitalRead(button2);
  state3 = digitalRead(button3);

  // Send the correct button-info and after that wait a bit to prevend button debounce
  if(state0 == HIGH) {
    ble.write("b0");
    delay(200);
  }
  else if (state1 == HIGH) {
    ble.write("b1");
    delay(200);
  }
  else if (state2 == HIGH) {
    ble.write("b2");
    delay(200);
  }
  else if (state3 == HIGH) {
    ble.write("b3");
    delay(200);
  }
}

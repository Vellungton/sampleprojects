package arvosanaGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import koulu.Koulu;
import koulu.Luokka;
import koulu.Oppilas;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

/**
 * T�ll� luokalla voit luoda tai muokata luokan tietoja.
 * @author vmoja
 * @versio 26.6.2014
 *
 */
@SuppressWarnings("unused")
public class Muokkaus extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private final JLabel lblAnnaLuokanNimi = new JLabel("Anna luokan nimi:");
    final JTextField luokanNimi = new JTextField();
    private final JButton btnTallenna = new JButton("Tallenna tiedot ja palaa arvosanoihin");
    private final JScrollPane scrollPaneOppilaidenNimet = new JScrollPane();
    private final JButton btnLisaaOppilas = new JButton("Lis\u00E4\u00E4 oppilas");
    private final JTextField lisaaOppilasKentta = new JTextField();
    private final JButton btnPoistaOppilas = new JButton("Poista oppilas");
    @SuppressWarnings("rawtypes")
    private final JList list = new JList();
    
    private Koulu koulu;
    private Luokka luokka;
    private DefaultListModel<Oppilas> oppilasLista = new DefaultListModel<Oppilas>();

    /**
     * Create the frame.
     * @param koulu 
     * @param luokka jos ollaan luomassa uutta, niin t�m� on viel� tyhj�, muuten sis�lt�� luokan tiedon
     */
    @SuppressWarnings({ "unchecked" })
    public Muokkaus(Koulu koulu, Luokka luokka) {
        this.koulu = koulu;
        this.luokka = luokka;
        laitaOppilaatListaan(luokka.getId());
        
        setTitle("Muokkaa luokan tietoja");
        lisaaOppilasKentta.setBounds(38, 75, 134, 28);
        lisaaOppilasKentta.setColumns(10);
        luokanNimi.setBounds(190, 35, 134, 28);
        luokanNimi.setColumns(10);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        lblAnnaLuokanNimi.setBounds(53, 41, 125, 16);
        
        contentPane.add(lblAnnaLuokanNimi);
        contentPane.add(luokanNimi);
        if (!luokka.toString().equals("")){
            luokanNimi.setText(luokka.toString());
            luokanNimi.setEditable(false);
        }
        luokanNimi.setText(luokka.toString());
        
        btnTallenna.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (luokanNimi.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Anna luokalle nimi, niin l�yd�t sen paremmin!");
                    return;
                }
                luoUusiLuokantiedot();
            }
        });
        btnTallenna.setBounds(38, 229, 394, 29);
        
        contentPane.add(btnTallenna);
        scrollPaneOppilaidenNimet.setBounds(197, 75, 235, 142);
        contentPane.add(scrollPaneOppilaidenNimet);
        
        list.setModel(oppilasLista);
        scrollPaneOppilaidenNimet.setViewportView(list);
        
        btnLisaaOppilas.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lisaaOppilas();
            }
        });
        btnLisaaOppilas.setBounds(38, 115, 117, 29);
        
        contentPane.add(btnLisaaOppilas);
        
        contentPane.add(lisaaOppilasKentta);
        btnPoistaOppilas.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                poistaValittuOppilas();
            }
        });
        btnPoistaOppilas.setBounds(38, 156, 117, 29);
        
        contentPane.add(btnPoistaOppilas);
        
        koulu.tallennaLuokat();
    }
    
    // Omaa koodia

    /**
     * Laittaa kaikki luokan oppilaat listaan
     * @param luokanId
     */
    private void laitaOppilaatListaan(int luokanId) {
        oppilasLista.clear();
            ArrayList<Oppilas> oppilaat = koulu.annaLuokanOppilaat(luokanId);
            if (!oppilaat.isEmpty()){
                for (Oppilas oppilas : oppilaat) {
                    oppilasLista.addElement(oppilas);
            } 
            }
    }

    /**
     * Luo uuden Luokantiedot-ikkunan ja vie parametrina luokan ja koulun
     */
    protected void luoUusiLuokantiedot() {
        luokka.setNimi(luokanNimi.getText());
        Luokantiedot uusiLuokantiedot = new Luokantiedot(koulu, luokka);
        uusiLuokantiedot.setVisible(true);
        suljeVanha();
    }


    /**
     * Poistaa listasta valitun oppilaan tietorakenteesta ja t�m�n j�lkeen hakee uudelleen oppilaat listaan
     */
    protected void poistaValittuOppilas() {
        koulu.poista(((Oppilas) list.getSelectedValue()));
        laitaOppilaatListaan(luokka.getId());
    }

    /**
     * Lis�t��n uusi oppilas tietorakenteeseen
     */
    protected void lisaaOppilas() {
        oppilasLista.addElement(koulu.luoUusiOppilas(lisaaOppilasKentta.getText(), luokka.getId()));
    }

    /**
     * H�vitet��n t�m� ikkuna
     */
    protected void suljeVanha() {
        this.dispose();
    }
}

package arvosanaGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import koulu.Koulu;
import koulu.Luokka;

import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

/**
 * T�m� ikkuna tulee n�kyviin ohjelman avatessa. Voit valita jo olemassa olevan luokan tai tehd� kokonaan uuden.
 * @author vmoja
 * @versio 27.6.2014
 *
 */
@SuppressWarnings("unused")
public class Aloitusikkuna extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private final JPanel panelTervehdys = new JPanel();
    private final JLabel lblTervetuloa = new JLabel("Tervetuloa k\u00E4ytt\u00E4m\u00E4\u00E4n arvosanojen hallintaohjelmaa!");
    private final JButton btnLuoUusiLuokka = new JButton("Luo uusi luokka");
    @SuppressWarnings("rawtypes")
    private final JComboBox comboBoxLuokat = new JComboBox();
    private final JLabel lblValitseLuokkaTai = new JLabel("Valitse luokka listasta");
    
    private Koulu koulu;
    private DefaultComboBoxModel<Luokka> luokat = new DefaultComboBoxModel<Luokka>();
    private final JLabel lblTaiHaeLuokkaa = new JLabel("Tai hae luokkaa nimell\u00E4");
    private final JTextField textFieldHakukentta = new JTextField();
    private final JScrollPane scrollPaneLuokkienNimet = new JScrollPane();
    @SuppressWarnings("rawtypes")
    private final JList listLuokkienNimet = new JList();
    
    private DefaultListModel<Luokka> luokkaLista = new DefaultListModel<Luokka>();
    private final JLabel lblLuoUusiLuokka = new JLabel("Luo uusi tyhj\u00E4 luokka");

    /**
     * Launch the application.
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Aloitusikkuna frame = new Aloitusikkuna(new Koulu());
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     * @param koulu hallinnointiluokka koulu
     */
    @SuppressWarnings({ "unchecked" })
    public Aloitusikkuna(Koulu koulu) {
        textFieldHakukentta.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                haeListaanLuokat();
            }
        });
        textFieldHakukentta.setBounds(252, 91, 134, 28);
        textFieldHakukentta.setColumns(10);
        this.koulu = koulu;
        setTitle("Arvosanat iskuun");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        panelTervehdys.setBounds(5, 5, 440, 26);
        
        contentPane.add(panelTervehdys);
        
        panelTervehdys.add(lblTervetuloa);
        btnLuoUusiLuokka.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                teeUusiMuokkausikkuna();
            }
        });
        btnLuoUusiLuokka.setBounds(23, 229, 130, 29);
        
        contentPane.add(btnLuoUusiLuokka);

        comboBoxLuokat.setModel(luokat);
        lueLuokat();
        comboBoxLuokat.setSelectedIndex(-1);
        comboBoxLuokat.setBounds(72, 93, 52, 27);
        
        comboBoxLuokat.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                luoUusiLuokantiedot((Luokka) e.getItem());
            }
        });
        
        contentPane.add(comboBoxLuokat);
        lblValitseLuokkaTai.setBounds(23, 65, 175, 16);
        
        contentPane.add(lblValitseLuokkaTai);
        lblTaiHaeLuokkaa.setBounds(242, 65, 154, 16);
        
        contentPane.add(lblTaiHaeLuokkaa);
        
        contentPane.add(textFieldHakukentta);
        scrollPaneLuokkienNimet.setBounds(252, 118, 134, 140);
        
        contentPane.add(scrollPaneLuokkienNimet);
        listLuokkienNimet.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                luoUusiLuokantiedotHakukentasta();
            }
        });
        
        
        
        listLuokkienNimet.setModel(luokkaLista);
        listLuokkienNimet.setSelectedIndex(-1);
        scrollPaneLuokkienNimet.setViewportView(listLuokkienNimet);
        lblLuoUusiLuokka.setBounds(23, 201, 146, 16);
        
        contentPane.add(lblLuoUusiLuokka);
    }
   
    

    // Omaa koodia
    

    /**
     * Luodaan uusi Luokantiedot-ikkuna, jolle vied��n parametrina listasta valittu Luokka-olio
     */
    protected void luoUusiLuokantiedotHakukentasta() {
        Luokantiedot uusiLuokantiedot = new Luokantiedot(koulu, (Luokka) listLuokkienNimet.getSelectedValue());
        uusiLuokantiedot.setVisible(true);
        suljeVanha();
    }    
    
    /**
     * Laitetaan listaan kaikki hakukent�n teksti� vastaavat Luokka-oliot
     */
    protected void haeListaanLuokat() {
        luokkaLista.clear();
        ArrayList<Luokka> haetutLuokat = koulu.haeMerkkijonollaLuokat(textFieldHakukentta.getText());
        if (!haetutLuokat.isEmpty()){
            for (Luokka luokka : haetutLuokat) {
                luokkaLista.addElement(luokka);
        } 
        }
        
    }


    /**
     * Lis�t��n comboBoxiin tiedostosta luetut luokat
     */
    @SuppressWarnings("unchecked")
    protected void lueLuokat() {
        @SuppressWarnings("hiding")
        ArrayList<Luokka> luokat = koulu.getLuokat();
        for (Luokka luokka : luokat) {
            comboBoxLuokat.addItem(luokka);
        }
        
    }

    /**
     * Tehd��n uusi Muokkaus-ikkuna, jolle vied��n parametrina luotu tyhj� Luokka-olio
     */
    protected void teeUusiMuokkausikkuna() {
        Luokka uusiLuokka = new Luokka("");
        koulu.luoUusiLuokka(uusiLuokka);
        Muokkaus uusiMuokkaus = new Muokkaus(koulu, uusiLuokka);
        uusiMuokkaus.setVisible(true);
        suljeVanha();
    }

    /**
     * Avataan Luokantiedot-ikkuna ja vied��n sille parametrina comboBoxista valitun luokan tiedot
     * @param luokka
     */
    protected void luoUusiLuokantiedot(Luokka luokka) {
        Luokantiedot uusiLuokantiedot = new Luokantiedot(koulu, luokka);
        uusiLuokantiedot.setVisible(true);
        suljeVanha();
    }

    /**
     * Sulkee t�m�n ikkunan
     */
    protected void suljeVanha() {
        this.dispose();
    }
}

package arvosanaGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.JMenuBar;
import javax.swing.JButton;

import java.awt.FlowLayout;

import javax.swing.SwingConstants;
import javax.swing.AbstractListModel;

import guilib.EditPanel;

import java.awt.Component;

import javax.swing.JMenu;
import javax.swing.UIManager;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;

import koulu.Koe;
import koulu.Koulu;
import koulu.Luokka;
import koulu.Oppilas;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

/**
 * Arvosanarekisterin p��ikkuna, jossa voit tarkastella yksitt�isten luokkien/oppilaiden arvosanoja.
 * @author vmoja
 * @versio 27.6.2014
 *
 */
@SuppressWarnings({ "serial", "unused" })
public class Luokantiedot extends JFrame {

    private JPanel contentPane;
    private final JSplitPane splitPaneLuokanTiedot = new JSplitPane();
    private final JPanel panelLista = new JPanel();
    private final JPanel panelListaOppilaista = new JPanel();
    private final JScrollPane scrollPaneOppilaat = new JScrollPane();
    @SuppressWarnings("rawtypes")
    private final JList listOppilaat = new JList();
    private final JLabel lblListaOppilaista = new JLabel("Oppilaat");
    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu mnTiedosto = new JMenu("Tiedosto");
    private final JMenu mnMuokkaa = new JMenu("Muokkaa");
    private final JMenu mnArvosteleKoe = new JMenu("Arvostele koe");
    private final JMenuItem mntmLisaaluokka = new JMenuItem("Lis\u00E4\u00E4 luokka");
    private final JMenuItem mntmMuokkaaLuokanTietoja = new JMenuItem("Muokkaa luokan tietoja");
    private final JMenuItem mntmArvosteleKoe = new JMenuItem("Arvostele koe");
    private final JMenuItem mntmTarkasteleToistaLuokkaa = new JMenuItem("Tarkastele toista luokkaa");
    private final JMenuItem mntmTallenna = new JMenuItem("Tallenna");
    private final JMenuItem mntmLopeta = new JMenuItem("Lopeta");    
    private final JPanel panelArvosanat = new JPanel();
    private final JPanel panelArvosanaTeksti = new JPanel();
    private final JLabel lblArvosanat = new JLabel("Arvosanat:");
    private final JScrollPane scrollPaneArvosanat = new JScrollPane();
    @SuppressWarnings("rawtypes")
    
    final JList listArvosanat = new JList();
    
    private DefaultListModel<Oppilas> oppilasLista = new DefaultListModel<Oppilas>();
    private DefaultListModel<String> arvosanaLista = new DefaultListModel<String>();
    
    private Koulu koulu;
    private Luokka luokka;


    
    
    /**
     * Create the frame.
     * @param koulu luotu koulu
     * @param luokka esitett�v� luokka
     */
    @SuppressWarnings({ "unchecked" })
    public Luokantiedot(final Koulu koulu, Luokka luokka) {
        this.koulu = koulu;
        this.luokka = luokka;
        laitaOppilaatListaan(luokka.getId());
        
        setTitle(luokka.getNimi());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 571, 376);
        
        setJMenuBar(menuBar);
        
        menuBar.add(mnTiedosto);
        mntmTarkasteleToistaLuokkaa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Aloitusikkuna uusiAloitusikkuna = new Aloitusikkuna(koulu);
                uusiAloitusikkuna.setVisible(true);
                suljeVanha();
            }
        });
        
        mnTiedosto.add(mntmTarkasteleToistaLuokkaa);
        mntmTallenna.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        mntmTallenna.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    tallenna();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        
        mnTiedosto.add(mntmTallenna);
        mntmLopeta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        mntmLopeta.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    koulu.tallenna();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                sammuta();
            }
        });
        
        mnTiedosto.add(mntmLopeta);
        
        menuBar.add(mnMuokkaa);
        mntmLisaaluokka.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                teeUusiMuokkausikkunaUusiLuokka();
            }
        });
        
        mnMuokkaa.add(mntmLisaaluokka);
        mntmMuokkaaLuokanTietoja.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                teeUusiMuokkausikkuna();
            }
        });
        
        mnMuokkaa.add(mntmMuokkaaLuokanTietoja);
        
        menuBar.add(mnArvosteleKoe);
        mntmArvosteleKoe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                teeUusiArvosteluikkuna();
            }
        });
        
        mnArvosteleKoe.add(mntmArvosteleKoe);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        contentPane.add(splitPaneLuokanTiedot, BorderLayout.CENTER);
        
        splitPaneLuokanTiedot.setLeftComponent(panelLista);
        panelLista.setLayout(new BorderLayout(0, 0));
        
        panelLista.add(panelListaOppilaista, BorderLayout.NORTH);
        panelListaOppilaista.setLayout(new BorderLayout(0, 0));
        
        panelListaOppilaista.add(lblListaOppilaista, BorderLayout.NORTH);
        
        panelLista.add(scrollPaneOppilaat, BorderLayout.CENTER);
        
        listOppilaat.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                listArvosanat.setSelectedIndex(-1);
                if (e.getValueIsAdjusting()){
                    listArvosanat.removeAll();
                    naytaValitunTiedot();
                }
                
            }
        });
        
        listOppilaat.setModel(oppilasLista);
        listOppilaat.setSelectedIndex(-1);
        scrollPaneOppilaat.setViewportView(listOppilaat);
        
        splitPaneLuokanTiedot.setRightComponent(panelArvosanat);
        panelArvosanat.setLayout(new BorderLayout(0, 0));
        
        panelArvosanat.add(panelArvosanaTeksti, BorderLayout.NORTH);
        panelArvosanaTeksti.add(lblArvosanat);
        panelArvosanat.add(scrollPaneArvosanat, BorderLayout.CENTER);
        scrollPaneArvosanat.setViewportView(listArvosanat);
    }


    // Omat koodit
    
    /**
     * Sammuttaa koko ohjelman
     */
    protected void sammuta() {
        System.exit(0);
    }


    /**
     * Tehd��n uusi Muokkaus-ikkuna ja luodaan uusi Luokka
     */
    protected void teeUusiMuokkausikkunaUusiLuokka() {
        Luokka uusi = new Luokka("");
        koulu.luoUusiLuokka(uusi);
        Muokkaus uusiMuokkaus = new Muokkaus(koulu, uusi);
        uusiMuokkaus.setVisible(true);
        suljeVanha();
    }

    /**
     * Tehd��n uusi Muokkaus-ikkuna ja vied��n parametrina jo k�yt�ss� oleva luokka
     */
    protected void teeUusiMuokkausikkuna() {
        Muokkaus muokkaus = new Muokkaus(koulu, luokka);
        muokkaus.setVisible(true);
        suljeVanha();
    }


    /**
     * Tallennetaan kaikki
     * @throws IOException
     */
    protected void tallenna() throws IOException {
        koulu.tallenna();
    }


    /**
     * N�ytet��n valitun oppilaan arvosanatiedot
     */
    @SuppressWarnings("unchecked")
    protected void naytaValitunTiedot() {
        listArvosanat.setModel(arvosanaLista);
        listArvosanat.setSelectedIndex(-1);
        arvosanaLista.clear();
        lisaaKaikkiKokeetListaan((Oppilas) listOppilaat.getSelectedValue());
        
    }

    /**
     * Haetaan oppilaan kokeet ja lis�t��n ne listaan
     * @param oppilas
     */
    protected void lisaaKaikkiKokeetListaan(Oppilas oppilas) {
        ArrayList<String> arvosanat = koulu.tulostaTamanKaikkiArvosanat(oppilas.getId());
        for (String string : arvosanat) {
            arvosanaLista.addElement(string);
        }        
    }


    /**
     * Laitetaan kaikki luokan oppilaat listaan
     * @param luokanId
     */
    private void laitaOppilaatListaan(int luokanId) {
        ArrayList<Oppilas> oppilaat = koulu.annaLuokanOppilaat(luokanId);
        Collections.sort(oppilaat);
        if (!oppilaat.isEmpty()){
            for (Oppilas oppilas : oppilaat) {
                oppilasLista.addElement(oppilas);
        } 
        }
}

    /**
     * Luodaan uusi Koe-olio ja uusi Arvostelu-ikkuna
     */
    protected void teeUusiArvosteluikkuna() {
        Arvostelu uusiArvostelu = new Arvostelu(koulu, luokka, new Koe("",0));
        uusiArvostelu.setVisible(true);
        suljeVanha();
    } 

    /**
     * Suljetaan t�m� vanha ikkuna
     */
    protected void suljeVanha() {
        this.dispose();
    }

}

package arvosanaGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import koulu.Koe;
import koulu.Koulu;
import koulu.Luokka;
import koulu.Oppilas;

/**
 * T�ss� luokassa arvostellaan luokalle kokeita.
 * @author vmoja
 * @versio 27.6.2014
 *
 */
@SuppressWarnings("unused")
public class Arvostelu extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPaneKokoTaulu;
    private final JPanel panelAlaPaneeli = new JPanel();
    private final JButton btnTallennaArvosanatJa = new JButton("Tallenna arvosanat ja palaa tarkastelemaan luokan tietoja");
    private final JSplitPane splitPane = new JSplitPane();
    private final JScrollPane scrollPaneOppilaat = new JScrollPane();
    @SuppressWarnings("rawtypes")
    private final JList listOppilaat = new JList();
    private final JPanel panelPisteet = new JPanel();
    private final JTextField textFieldOppilaanPisteet = new JTextField();
    private final JButton btnTallennaPisteet = new JButton("Tallenna pisteet");
    private final JSplitPane splitPanelKokeenNimi = new JSplitPane();
    private final JSplitPane splitPaneMaksimipisteet = new JSplitPane();
    private final JSplitPane splitPaneKokeenNimi = new JSplitPane();
    private final JLabel lblMaksimipisteet = new JLabel("Maksimipisteet:");
    private final JTextField textFieldMaksimipisteet = new JTextField();
    private final JLabel lblKokeenNimi = new JLabel("Kokeen nimi:");
    private final JTextField textFieldKokeenNimi = new JTextField();
    
    private Koulu koulu;
    private Luokka luokka;
    private Koe koe;
    private DefaultListModel<Oppilas> oppilasLista = new DefaultListModel<Oppilas>();

    /**
     * Create the frame.
     * @param koulu 
     * @param luokka 
     * @param koe 
     */
    @SuppressWarnings({ "unchecked" })
    public Arvostelu(Koulu koulu, Luokka luokka, Koe koe) {
        this.koulu = koulu;
        this.luokka = luokka;
        this.koe = koe;
        laitaOppilaatListaan(luokka.getId());
        
        textFieldKokeenNimi.setColumns(10);
        textFieldMaksimipisteet.setColumns(10);
        setTitle(luokka.getNimi());
        textFieldOppilaanPisteet.setBounds(6, 6, 134, 28);
        textFieldOppilaanPisteet.setColumns(10);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 557, 426);
        contentPaneKokoTaulu = new JPanel();
        contentPaneKokoTaulu.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPaneKokoTaulu);
        contentPaneKokoTaulu.setLayout(new BorderLayout(0, 0));
        
        contentPaneKokoTaulu.add(panelAlaPaneeli, BorderLayout.SOUTH);
        btnTallennaArvosanatJa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    teeUusiLuokantiedot();
                } catch (Exception e2) {
                    JOptionPane.showMessageDialog(null, "Joku meni pieleen!!!\nTarkista:\n"
                            + "1. Onko kokeelle annettu kokonaislukuina maksimipisteem��r�\n"
                            + "2. Onko kokeelle annettu nimi."); 
                }
            }
        });
        
        panelAlaPaneeli.add(btnTallennaArvosanatJa);
        
        contentPaneKokoTaulu.add(splitPane, BorderLayout.CENTER);
        
        splitPane.setLeftComponent(scrollPaneOppilaat);
        
        listOppilaat.setModel(oppilasLista);
        listOppilaat.setSelectedIndex(0);
        scrollPaneOppilaat.setViewportView(listOppilaat);
        
        splitPane.setRightComponent(panelPisteet);
        panelPisteet.setLayout(null);
        
        panelPisteet.add(textFieldOppilaanPisteet);
        btnTallennaPisteet.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tallennaOppilaanPisteet();
            }
        });
        btnTallennaPisteet.setBounds(6, 46, 134, 29);
        
        panelPisteet.add(btnTallennaPisteet);
        
        contentPaneKokoTaulu.add(splitPanelKokeenNimi, BorderLayout.NORTH);
        
        splitPanelKokeenNimi.setLeftComponent(splitPaneMaksimipisteet);
        
        splitPaneMaksimipisteet.setLeftComponent(lblMaksimipisteet);
        
        splitPaneMaksimipisteet.setRightComponent(textFieldMaksimipisteet);
        
        splitPanelKokeenNimi.setRightComponent(splitPaneKokeenNimi);
        
        splitPaneKokeenNimi.setLeftComponent(lblKokeenNimi);
        
        splitPaneKokeenNimi.setRightComponent(textFieldKokeenNimi);
    }
    

    //   Omaa koodia
    
    /**
     * Laitetaan parametrina saadun luokan idn oppilaat listaan
     * @param luokanId
     */
    private void laitaOppilaatListaan(int luokanId) {
        ArrayList<Oppilas> oppilaat = koulu.annaLuokanOppilaat(luokanId);
        if (!oppilaat.isEmpty()){
            for (Oppilas oppilas : oppilaat) {
                oppilasLista.addElement(oppilas);
        } 
        }
}

    /**
     * Asetetaan kokeelle nimi ja maksimipisteet ja lis�t��n se tietorakenteeseen
     * Luodaan uusi Luokantiedot-ikkuna ja suljetaan t�m� ikkuna
     */
    protected void teeUusiLuokantiedot() {
        koe.setMaksimi(Integer.parseInt(textFieldMaksimipisteet.getText()));
        koe.setNimi(textFieldKokeenNimi.getText());
        koulu.lisaaKoe(koe);
        Luokantiedot uusiLuokantiedot = new Luokantiedot(koulu, luokka);
        uusiLuokantiedot.setVisible(true);
        suljeVanha();
    }

    /**
     * H�vitet��n t�m� ikkuna
     */
    protected void suljeVanha() {
        this.dispose();
    }

    /**
     * Tallennetaan listasta valitun oppilaan pisteet arvosana tietokantaan
     */
    protected void tallennaOppilaanPisteet() {
        koulu.luoUusiArvosana(((Oppilas) listOppilaat.getSelectedValue()).getId(), koe.getId(), Integer.parseInt(textFieldOppilaanPisteet.getText()));
    }
}

package koulu;

/**
 * Vesan visioima poikkeusluokka
 * @author vmoja
 * @versio 23.7.2014
 *
 */
public class SailoException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Luodaan poikkeusluokan olio
     * @param viesti
     */
    public SailoException(String viesti){
        super(viesti);
    }
    

}

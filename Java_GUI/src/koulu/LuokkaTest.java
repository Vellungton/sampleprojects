package koulu;
// Generated by ComTest BEGIN
import static org.junit.Assert.*;
import org.junit.*;
import koulu.*;
// Generated by ComTest END

/**
 * Test class made by ComTest
 * @version 2014.07.25 06:59:15 // Generated by ComTest
 *
 */
public class LuokkaTest {



  // Generated by ComTest BEGIN
  /** testGetId40 */
  @Test
  public void testGetId40() {    // Luokka: 40
    Luokka vanha = new Luokka("ysit", 99);  // T?t? id:llist? luontiahan k?ytet??n vain ohjelman alussa!
    Luokka uusi = new Luokka("kutoset"); 
    assertEquals("From: Luokka line: 43", 99, vanha.getId()); 
    assertEquals("From: Luokka line: 44", 100, uusi.getId()); 
    vanha.setNimi("vanha"); 
    assertEquals("From: Luokka line: 46", "vanha", vanha.getNimi()); 
    assertEquals("From: Luokka line: 47", "kutoset", uusi.toString()); 
  } // Generated by ComTest END
}
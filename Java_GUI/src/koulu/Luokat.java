package koulu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

    /**
     * @author vmoja
     * @versio 16.7.2014
     *
     */
    public class Luokat {
        private ArrayList<Luokka> alkiot;
        
        /**
         * Konstruktori Luokat-luokalle
         */
        public Luokat () {
            alkiot = new ArrayList<Luokka>();
            lueTallennetut();
        }
        
        /**
         * Luetaan tiedostosta jo mahdolliset luokat
         */
        public void lueTallennetut() {
            try {
                File luokkienNimet = new File("luokkienNimet.txt");
                @SuppressWarnings("resource")
                Scanner lukija = new Scanner(luokkienNimet, "UTF-8");
                while (lukija.hasNextLine()) {
                    String rivi = lukija.nextLine();
                    //System.out.println(rivi);
                    String[] relaatiot = rivi.split("\\|");
                    try {
                        luoUusiLuokka(relaatiot[1], Integer.parseInt(relaatiot[0]));
                    } catch (Exception e) {
                        System.out.println("ei luo");
                    }
                    
                }
                lukija.close();
            } catch (Exception e) {
                System.out.println("ei toiminut");
            }   
        }        
           
    /**
     * @param i indeksinumero
     * @return haettu Luokka
     */
    public Luokka anna(int i) {
        if (i < 0 || i > (alkiot.size()-1)){
            throw new IndexOutOfBoundsException("Laiton indeksi: " + i);
        }
        return alkiot.get(i);
    }
    
    /**
     * Palauttaa ArrayListin luokista
     * @return lista luokista
     */
    public ArrayList<Luokka> getLuokat() {
        return alkiot;
    }


    /**
     * Palauttaa luokkien lukum��r�n
     * @return luokkien lukum��r�
     * @example
     * <pre name="test">
     * Luokat uusi = new Luokat();
     * uusi.getLkm() === 3;
     * uusi.luoUusiLuokka("lukion eka");
     * uusi.getLkm() === 4;
     * </pre>
     */
    public int getLkm() {
        return alkiot.size();
    }
    
    /**
     * Luo uuden Luokan ja lis�� sen Luokat-luokan tietorakenteeseen
     * @param nimi luokan nimi
     */
    public void luoUusiLuokka(String nimi) {
        try {
            lisaa(new Luokka(nimi));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Luo uuden Luokan ja lis�� sen Luokat-luokan tietorakenteeseen
     * @param nimi luokan nimi
     * @param luokanId 
     */
    public void luoUusiLuokka(String nimi, int luokanId) {
        try {
            lisaa(new Luokka(nimi, luokanId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lis�� uuden luokan tietorakenteeseen. Ottaa luokan omistukseensa.
     * Huom! Tietorakenne muuttuu omistajaksi.
     * @param luokka
     */
    public void lisaa(Luokka luokka){
        alkiot.add(luokka);
    }

    /**
     * Tallentaa luokan tietorakenteen
     * @throws IOException
     */
    public void tallenna() throws IOException {
        @SuppressWarnings("resource")
        FileWriter kirjoittaja = new FileWriter("luokkienNimet.txt");
        for (Luokka luokka : alkiot) {
            kirjoittaja.write(luokka.getId()+"|"+luokka.getNimi()+"\n");
        }
        kirjoittaja.close();
    }

    /**
     * Haetaan kaikki parametrina saatua merkkijonoa vastaavat Luokka-oliot ja lis�t��n listaan
     * Lopuksi lista l�hetet��n eteenp�in
     * @param text merkkijono
     * @return merkkijonon sis�lt�v�t alkiot
     */
    public ArrayList<Luokka> haeMerkkijonollaLuokat(String text) {
        ArrayList<Luokka> haetutLuokat = new ArrayList<Luokka>();
        for (Luokka luokka : alkiot) {
            if (luokka.getNimi().toLowerCase().contains(text.toLowerCase())){
                haetutLuokat.add(luokka);
            }
        }
        return haetutLuokat;
    }
    
    }
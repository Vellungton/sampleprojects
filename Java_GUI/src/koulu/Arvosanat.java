package koulu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

    
    /**
     * Luokka arvosanojen säilömiseen ja käsittelyyn
     * @author vmoja
     * @versio 16.7.2014
     *
     */
    public class Arvosanat {
        private ArrayList<Arvosana> alkiot;    
        
        /**
         * Konstruktori Arvosanat-luokalle
         */
        public Arvosanat(){
            this.alkiot = new ArrayList<Arvosana>();
            lueTallennetut();
        }
        
        /**
         * Luetaan tiedostosta jo mahdolliset luokat
         */
        public void lueTallennetut() {
            try {
                File arvosanat = new File("arvosanat.txt");
                @SuppressWarnings("resource")
                Scanner lukija = new Scanner(arvosanat, "UTF-8");
                while (lukija.hasNextLine()) {
                    String rivi = lukija.nextLine();
                    // System.out.println(rivi);
                    String[] relaatiot = rivi.split("\\|");
                    try {
                        luoUusiArvosana(Integer.parseInt(relaatiot[0]), Integer.parseInt(relaatiot[1]), Integer.parseInt(relaatiot[2]));
                    } catch (Exception e) {
                        System.out.println("ei luo");
                    }
                    
                }
                lukija.close();
            } catch (Exception e) {
                System.out.println("ei toiminut");
            }   
        }          
    
    
        /**
         * Antaa arvosanan tiedot
         * @param i
         * @return arvosana olion
         */
        public Arvosana anna (int i) {
            if (i < 0 || (i > alkiot.size()-1)){
                throw new IndexOutOfBoundsException("Laiton indeksi: " + i);
            }
            return alkiot.get(i);
        }
    
    
    /**
     * Palauttaa arvosanojen lukumäärän
     * @return arvosanojen lukumäärä
     * @example
     * <pre name="test">
     * Arvosanat arvosanat = new Arvosanat();
     * arvosanat.getLkm() === 3;
     * arvosanat.luoUusiArvosana(1,2,3);
     * arvosanat.luoUusiArvosana(4,5,6);
     * arvosanat.getLkm() === 5;
     * arvosanat.luoUusiArvosana(7,8,9);
     * arvosanat.getLkm() === 6;
     * </pre>
     */
    public int getLkm() {
        return alkiot.size();
    }
    
    /**
     * Luodaan uusi arvosana
     * @param oppilaanId 
     * @param omatPisteet 
     * @param kokeenId
     */
    public void luoUusiArvosana(int oppilaanId, int kokeenId, int omatPisteet) {
        try {
            alkiot.add(new Arvosana(oppilaanId, omatPisteet, kokeenId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Etsii ja palauttaa oppilaan saaman pistemäärän parametrina tuodusta kokeesta.
     * @param kokeenId
     * @return oppilaan pistemäärä
     */
    public int getOmaPistemaara(int kokeenId){
        for (Arvosana arvosana : alkiot) {
            if (arvosana.getKokeenId()==kokeenId){
                return arvosana.getOmatPisteet();
            }
        }
        return 0;
    }

    /**
     * @param oppilaanId
     * @return lista kokeiden id numeroista
     */
    public ArrayList<Arvosana> getOppilaanArvosanat(int oppilaanId) {
        ArrayList<Arvosana>listaKokeista = new ArrayList<Arvosana>();
        for (Arvosana arvosana : alkiot) {
            if (arvosana.getOppilaanId() == oppilaanId){
                listaKokeista.add(arvosana);
            }
        }
        return listaKokeista;
    }
    
    /**
     * Tallentaa luokan tietorakenteen
     * @throws IOException
     */
    public void tallenna() throws IOException {
        @SuppressWarnings("resource")
        FileWriter kirjoittaja = new FileWriter("arvosanat.txt");
        for (Arvosana arvosana : alkiot) {
            kirjoittaja.write(arvosana.getOppilaanId()+"|"+arvosana.getKokeenId()+"|"+arvosana.getOmatPisteet()+"\n");
        }
        kirjoittaja.close();
    }
    
    
    }

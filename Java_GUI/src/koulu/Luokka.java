package koulu;

/**
 * Yksitt�isen luokan luokka
 * @author vmoja
 * @versio 15.7.2014
 *
 */
public class Luokka {
    private String nimi;
    private int id;
    private static int seuraavaNumero = 1;
    
    /**
     * Alustetaan luokka
     * Luodaan luokka
     * @param nimi
     */
    public Luokka(String nimi) {
        this.nimi = nimi;
        this.id = seuraavaNumero;
        seuraavaNumero++;
    }
    
    /**
     * Luodaan uusi luokka idn kera
     * @param nimi
     * @param id
     */
    public Luokka(String nimi, int id){
        this.nimi = nimi;
        this.id = id;
        seuraavaNumero = (id+1);
    }
    
    /**
     * Palauttaa luokan id numeron
     * @return luokan id
     * @example
     * <pre name="test">
     * Luokka vanha = new Luokka("ysit", 99);  // T�t� id:llist� luontiahan k�ytet��n vain ohjelman alussa!
     * Luokka uusi = new Luokka("kutoset");
     * vanha.getId() === 99;
     * uusi.getId() === 100;
     * vanha.setNimi("vanha");
     * vanha.getNimi() === "vanha";
     * uusi.toString() === "kutoset";
     * </pre>
     */
    public int getId(){
        return id;
    }
    
    /**
     * Asettaa nimen luokalle
     * @param nimi
     */
    public void setNimi(String nimi){
        this.nimi = nimi;
    }
    
    /**
     * Palauttaa luokan nimen
     * @return luokan nimi
     */
    public String getNimi(){
        return nimi;
    }
    
    public String toString(){
        return nimi;
    }
    

}

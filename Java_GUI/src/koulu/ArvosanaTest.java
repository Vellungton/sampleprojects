package koulu;
// Generated by ComTest BEGIN
import static org.junit.Assert.*;
import org.junit.*;
import koulu.*;
// Generated by ComTest END

/**
 * Test class made by ComTest
 * @version 2014.07.25 06:23:39 // Generated by ComTest
 *
 */
public class ArvosanaTest {



  // Generated by ComTest BEGIN
  /** testGetOppilaanId30 */
  @Test
  public void testGetOppilaanId30() {    // Arvosana: 30
    Arvosana uusi = new Arvosana(111, 12, 12); 
    assertEquals("From: Arvosana line: 32", 111, uusi.getOppilaanId()); 
  } // Generated by ComTest END


  // Generated by ComTest BEGIN
  /** testGetKokeenId43 */
  @Test
  public void testGetKokeenId43() {    // Arvosana: 43
    Arvosana uusi = new Arvosana(111, 111, 12); 
    assertEquals("From: Arvosana line: 45", 12, uusi.getKokeenId()); 
  } // Generated by ComTest END


  // Generated by ComTest BEGIN
  /** testGetOmatPisteet56 */
  @Test
  public void testGetOmatPisteet56() {    // Arvosana: 56
    Arvosana uusi = new Arvosana(111, 400, 12); 
    assertEquals("From: Arvosana line: 58", 400, uusi.getOmatPisteet()); 
  } // Generated by ComTest END
}
package koulu;

/**
 * Luokka yksittäistä arvosanaa varten
 * @author vmoja
 * @versio 16.7.2014
 *
 */
public class Arvosana {
    private int oppilaanId;
    private int omatPisteet;
    private int kokeenId;
    
    /**
     * Luodaan arvosana
     * @param oppilaanId 
     * @param omatPisteet
     * @param kokeenId
     */
    public Arvosana(int oppilaanId, int omatPisteet, int kokeenId) {
        this.oppilaanId = oppilaanId;
        this.omatPisteet = omatPisteet;
        this.kokeenId = kokeenId;
    }
    
    /**
     * Palauttaa oppilaan idn
     * @return oppilaan id
     * @example
     * <pre name="test">
     * Arvosana uusi = new Arvosana(111, 12, 12);
     * uusi.getOppilaanId() === 111;
     * </pre>
     */
    public int getOppilaanId(){
        return oppilaanId;
    }
    
    /**
     * Palauttaa kokeen id numeron.
     * @return kokeen id
     * @example
     * <pre name="test">
     * Arvosana uusi = new Arvosana(111, 111, 12);
     * uusi.getKokeenId() === 12;
     * </pre>
     */
    public int getKokeenId(){
        return kokeenId;
    }
    
    /**
     * Palauttaa arvosanan oman pistemäärän
     * @return oman pistemäärän
     * @example
     * <pre name="test">
     * Arvosana uusi = new Arvosana(111, 400, 12);
     * uusi.getOmatPisteet() === 400;
     * </pre>
     */
    public int getOmatPisteet(){
        return omatPisteet;
    }
    
    
}

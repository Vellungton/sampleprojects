package koulu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
    
    /**
     * @author vmoja
     * @versio 16.7.2014
     *
     */
    public class Kokeet {
        private static final int MAX_KOKEITA = 5;
        private int lkm = 0;
        private Koe[] alkiot = new Koe[MAX_KOKEITA];    
    
        
        /**
         * Luodaan Kokeet-luokka
         */
        public Kokeet(){
            lueTallennetut();
        }
        
        /**
         * Luetaan tiedostosta jo mahdolliset luokat
         */
        public void lueTallennetut() {
            try {
                File kokeidenNimet = new File("kokeidenNimet.txt");
                @SuppressWarnings("resource")
                Scanner lukija = new Scanner(kokeidenNimet, "UTF-8");
                while (lukija.hasNextLine()) {
                    String rivi = lukija.nextLine();
                    // System.out.println(rivi);
                    String[] relaatiot = rivi.split("\\|");
                    try {
                        luoUusiKoe(Integer.parseInt(relaatiot[0]), relaatiot[1], Integer.parseInt(relaatiot[2]));
                    } catch (Exception e) {
                        System.out.println("ei luo");
                    }
                    
                }
                lukija.close();
            } catch (Exception e) {
                System.out.println("ei toiminut");
            }   
        }
    /**
     * @param i indeksinumero
     * @return haettu Koe
     */
    public Koe anna(int i) {
        if (i < 0 || lkm <= i){
            throw new IndexOutOfBoundsException("Laiton indeksi: " + i);
        }
        return alkiot[i];
    }
    
    /**
     * Palauttaa kokeen maksimipisteet
     * @param kokeenId haetun kokeen id
     * @return vastaavaa idt� maksimipisteet, jos ei l�ydy, niin palautetaan nolla
     */
    public int getKokeenMaksimipisteet(int kokeenId){
        for (int i = 0; i < alkiot.length; i++) {
            if (alkiot[i] != null){
            if (alkiot[i].getId() == kokeenId){
                return alkiot[i].maksimi();
            }
            }
        }
        return 0;
    }
    
    /**
     * Palauttaa kokeen nimen
     * @param kokeenId haetun kokeen id
     * @return vastaavaa idt� nimi, jos ei l�ydy, niin palautetaan tyhj� merkkijono
     */
    public String getKokeenNimi(int kokeenId){
        for (int i = 0; i < alkiot.length; i++) {
            if (alkiot[i] != null){
            if (alkiot[i].getId() == kokeenId){
                return alkiot[i].getNimi();
            }
            }
        }
        return "";
    }
    
    
    /**
     * Palauttaa kokeiden lukum��r�n
     * @return kokeiden lukum��r�
     * @example
     * <pre name="test">
     * Kokeet kokeet = new Kokeet();
     * kokeet.getLkm() === 2;
     * kokeet.luoUusiKoe("koe", 100);
     * kokeet.luoUusiKoe(33, "Roy", 100);
     * kokeet.getLkm() === 4;
     * kokeet.luoUusiKoe("Aiserman", 100);
     * kokeet.luoUusiKoe("Fetorov", 100);
     * kokeet.getLkm() === 6;      // T�m� sen takia, ett� testaan samalla taulukon "kasvamista"
     * </pre>
     */
    public int getLkm() {
        return lkm;
    }
    
    /**
     * Luodaan uusi koe
     * @param nimi kokeen nimi
     * @param maksimipisteet kokeen maksimipistem��r�
     */
    public void luoUusiKoe(String nimi, int maksimipisteet) {
        try {
            lisaa(new Koe(nimi, maksimipisteet));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Luodaan uusi koe, mukaan luodaan jo valmiiksi id. K�ytet��n tiedoston avaamisen yhteydess�.
     * @param kokeenId
     * @param nimi
     * @param maksimipisteet
     */
    public void luoUusiKoe(int kokeenId, String nimi, int maksimipisteet) {
        try {
            lisaa(new Koe(kokeenId, nimi, maksimipisteet));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Lis�� uuden luokan tietorakenteeseen. Ottaa luokan omistukseensa.
     * Huom! Tietorakenne muuttuu omistajaksi.
     * Jos lkm kasvaa suuremmaksi kuin alkioiden m��r�, kopioidaan vanha taulukko ja luodaan uusi isompi tilalle vanhoilla tiedoilla.
     * @param koe
     */
    public void lisaa(Koe koe) {
        if (lkm >= alkiot.length){
            Koe isompi[] = new Koe [lkm+10];
            for (int i = 0; i < alkiot.length; i++) {
                isompi[i]=alkiot[i];
            }
            isompi[lkm] = koe;
            alkiot = isompi; 
            lkm++;
        } else {
            alkiot[lkm] = koe;
            lkm++;  
        }
        
    }

    /**
     * Tallentaa luokan tietorakenteen
     * @throws IOException
     */
    public void tallenna() throws IOException {
        @SuppressWarnings("resource")
        FileWriter kirjoittaja = new FileWriter("kokeidenNimet.txt");
        for (int i = 0; i < alkiot.length; i++) {
            if (alkiot[i] != null){
                kirjoittaja.write(alkiot[i].getId()+"|"+alkiot[i].getNimi()+"|"+alkiot[i].maksimi()+"\n");
            }
        }
        kirjoittaja.close();
    }
    
    }
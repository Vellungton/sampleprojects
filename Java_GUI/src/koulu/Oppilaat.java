package koulu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Pit�� yll� varsinaista rekisteri� oppilaista.
 * Lukee ja kirjoittaa oppilaidenNimet.dat fileen.
 * Osaa etsi� ja lajitella.
 * @author vmoja
 * @versio 4.7.2014
 *
 */
public class Oppilaat {
      private ArrayList<Oppilas> alkiot;
      
      /**
     * 
     */
    public Oppilaat(){
          this.alkiot = new ArrayList<Oppilas>();
          lueTallennetut();
      }
    
    
    /**
     * Luetaan tiedostosta jo mahdolliset luokat
     */
    public void lueTallennetut() {
        try {
            File oppilaidenNimet = new File("oppilaidenNimet.txt");
            @SuppressWarnings("resource")
            Scanner lukija = new Scanner(oppilaidenNimet, "UTF-8");
            while (lukija.hasNextLine()) {
                String rivi = lukija.nextLine();
                //System.out.println(rivi);
                String[] relaatiot = rivi.split("\\|");
                try {
                    luoUusiOppilas(relaatiot[1], Integer.parseInt(relaatiot[0]), Integer.parseInt(relaatiot[2]));
                } catch (Exception e) {
                    System.out.println("ei luo");
                }
                
            }
            lukija.close();
        } catch (Exception e) {
            System.out.println("ei toiminut");
        }   
    }     

    /**
     * Antaa oppilaan tiedot
     * @param i
     * @return oppilas olion
     */
    public Oppilas anna (int i) {
        if (i < 0 || (i > alkiot.size()-1)){
            throw new IndexOutOfBoundsException("Laiton indeksi: " + i);
        }
        return alkiot.get(i);
    }

    /**
     * Palauttaa oppilaiden lukum��r�n
     * @return oppilaiden lukum��r�
     * @example
     * <pre name="test">
     * Oppilaat uusi = new Oppilaat();
     * uusi.getLkm() === 5;
     * uusi.luoUusiOppilas("Jukka Poika", 12);
     * uusi.getLkm() === 6;
     * uusi.poista(uusi.anna(5));
     * uusi.getLkm() === 5;
     * 
     * </pre>
     */
    public int getLkm() {
        return alkiot.size();
    }
    
    /**
     * @param nimi
     * @param luokanId
     * @return Oppilas olion viite
     */
    public Oppilas luoUusiOppilas(String nimi, int luokanId) {
        Oppilas oppilas = new Oppilas(nimi, luokanId);
        try {
            lisaa(oppilas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oppilas;
    }
    
    /**
     * @param nimi
     * @param oppilaanId 
     * @param luokanId
     * @return Oppilas olion viite
     */
    public Oppilas luoUusiOppilas(String nimi, int oppilaanId, int luokanId) {
        Oppilas oppilas = new Oppilas(nimi, oppilaanId, luokanId);
        try {
            lisaa(oppilas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oppilas;
    }

    /**
     * Lis�� uuden oppilaan tietorakenteeseen. Ottaa oppilaan omistukseensa.
     * Huom! Tietorakenne muuttuu omistajaksi.
     * @param oppilas viite Oppilas olioon, joka lis�t��n tietorakenteeseen
     */
    public void lisaa(Oppilas oppilas) {
        alkiot.add(oppilas);
    }
    
    /**
     * K�y l�pi kaikki oppilaat ja palauttaa parametrina annetun luokan id-numeron oppilaat ArrayListiss�
     * @param luokanId
     * @return halutun luokan oppilaat listassa
     */
    public ArrayList<Oppilas> annaLuokanOppilaat(int luokanId){
        ArrayList<Oppilas> oppilaat = new ArrayList<Oppilas>();
        for (int i = 0; i < alkiot.size(); i++) {
            if (alkiot.get(i).getLuokanId() == luokanId) {
                oppilaat.add(alkiot.get(i));
            }
        }
        return oppilaat;
    }

    /**
     * Poistetaan listalta haettu oppilas
     * @param oppilas 
     */
    public void poista(Oppilas oppilas) {
        alkiot.remove(oppilas);
    }


    /**
     * Tallentaa luokan tietorakenteen
     * @throws IOException
     */
    public void tallenna() throws IOException {
        @SuppressWarnings("resource")
        FileWriter kirjoittaja = new FileWriter("oppilaidenNimet.txt");
        for (Oppilas oppilas : alkiot) {
            kirjoittaja.write(oppilas.getId()+"|"+oppilas.getNimi()+"|"+oppilas.getLuokanId()+"\n");
        }
        kirjoittaja.close();
    }

}

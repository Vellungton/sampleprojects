package koulu;

/**
 * Yksittäisen kokeen luokka
 * @author vmoja
 * @versio 16.7.2014
 *
 */
public class Koe {
    private String nimi;
    private int maksimi;
    private int id;
    private static int seuraavaNumero = 1;
    
    /**
     * Alustetaan koe
     * @param nimi
     * @param maksimipistemaara
     */
    public Koe(String nimi, int maksimipistemaara) {
        this.nimi = nimi;
        this.maksimi = maksimipistemaara;
        this.id = seuraavaNumero;
        seuraavaNumero++;
    }
    
    /**
     * Kuormitetaan konstruktoria ja luodaan kokeita.
     * @param id jo luodun kokeen id
     * @param nimi kokeen nimi
     * @param maksimipistemaara
     */
    public Koe(int id, String nimi, int maksimipistemaara){
        this.nimi = nimi;
        this.maksimi = maksimipistemaara;
        this.id = id;
        seuraavaNumero = (id+1);
    }
    
    /**
     * Palauttaa kokeen id:n
     * @return kokeen id
     * @example
     * <pre name="test">
     * Koe koe = new Koe(99, "Wayne", 66);
     * koe.getId() === 99;
     * koe.setNimi("Jagr");
     * koe.setMaksimi(88);
     * koe.getNimi() === "Jagr";
     * koe.maksimi() === 88;
     * </pre>
     */
    public int getId(){
        return id;
    }
    
    /**
     * Palauttaa kokeen nimen
     * @return kokeen nimi
     */
    public String getNimi(){
        return nimi;
    }
    
    /**
     * Asettaa nimen kokeelle
     * @param nimi
     */
    public void setNimi(String nimi){
        this.nimi = nimi;
    }
    
    /**
     * Asettaa maksimipisteet
     * @param maksimi
     */
    public void setMaksimi(int maksimi) {
        this.maksimi = maksimi;
    }
    
    /**
     * Palautaa kokeen maksimipisteet
     * @return maksimipisteet
     */
    public int maksimi(){
        return maksimi;
    }
    
    public String toString(){
        return nimi + ", maksimipistemäärä: " + maksimi;
    }
    
    
    
    

}

package koulu;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Koulu-luokka hallinnoi kaikkia muita luokkia ja toimii linkkin� GUIlle.
 * @author vmoja
 * @versio 16.7.2014
 *
 */
@SuppressWarnings("unused")
public class Koulu {
    private Oppilaat oppilaat;
    private Kokeet kokeet;
    private Luokat luokat;
    private Arvosanat arvosanat;
    
    /**
     *  Koulua luotaessa alustetaan kaikki s�il�luokat
     */
    public Koulu () {
        luokat = new Luokat();
        oppilaat = new Oppilaat();
        kokeet = new Kokeet();
        arvosanat = new Arvosanat();
    }

    /**
     * @param nimi
     * @param luokka
     * @return Oppilas viite
     */
    public Oppilas luoUusiOppilas(String nimi, int luokka){
        return oppilaat.luoUusiOppilas(nimi, luokka);
    }
    
    /**
     * @param taulukkoPaikka
     * @return oppilaan tiedot merkkijonona
     */
    public String tulostaOppilas(int taulukkoPaikka) {
        return oppilaat.anna(taulukkoPaikka).toString();
    }
    
    /**
     * Palauttaa oppilaiden lukum��r�n
     * @return oppilaiden lukum��r�
     */
    public int oppilaatGetLkm() {
        return oppilaat.getLkm();
    }
    
    /**
     * Luodaan uusi luokka
     * @param luokka
     */
    public void luoUusiLuokka(Luokka luokka) {
        luokat.lisaa(luokka);
    }
    
    /**
     * Luodaan uusi koe
     * @param nimi kokeen nimi
     * @param maksimipisteet on maksimipistem��r� mink� kokeesta voi saada
     */
    public void luoUusiKoe(String nimi, int maksimipisteet){
        kokeet.luoUusiKoe(nimi, maksimipisteet);
    }
    
    /**
     * Tulostetaan kokeen tiedot merkkijonona
     * @param taulukkoPaikka kokeen sijainti taulukossa
     * @return kokeen tiedot toString()ill�
     */
    public String tulostaKoe(int taulukkoPaikka){
        return kokeet.anna(taulukkoPaikka).toString();
    }
    
    /**
     * K�y l�pi kaikki oppilaat ja palauttaa parametrina annetun luokan id-numeron oppilaat ArrayListiss�
     * @param luokanId
     * @return halutun luokan oppilaat listassa
     */
    public ArrayList<Oppilas> annaLuokanOppilaat(int luokanId){
        return oppilaat.annaLuokanOppilaat(luokanId);
    }
    
    /**
     * Luo uuden arvosana-olion ja lis�� sen tietorakenteeseen
     * @param oppilaanId 
     * @param kokeenId
     * @param omaPistemaara
     */
    public void luoUusiArvosana(int oppilaanId, int kokeenId, int omaPistemaara){
        arvosanat.luoUusiArvosana(oppilaanId, kokeenId, omaPistemaara);
    }
    
    
    /**
     * Tulostaa merkkijonona tietyn arvosanan haetulle oppilaalle ja kokeelle
     * @param oppilaanId
     * @return merkkijonoesitys oppilaan kokeesta saamasta pistem��r�st�
     */
    public ArrayList<String> tulostaTamanKaikkiArvosanat(int oppilaanId){
        ArrayList<Arvosana> oppilaanArvosanat = arvosanat.getOppilaanArvosanat(oppilaanId);
        ArrayList<String> tulostus = new ArrayList<String>();
        for (Arvosana arvosana : oppilaanArvosanat) {
            tulostus.add(kokeet.getKokeenNimi(arvosana.getKokeenId()) + " " + arvosana.getOmatPisteet() + " pistett� " + kokeet.getKokeenMaksimipisteet(arvosana.getKokeenId()) + " pisteest�.");
        }
        
        return tulostus;
    }
    
    /**
     * Lis�t��n koe tietokantaan
     * @param koe
     */
    public void lisaaKoe(Koe koe){
        kokeet.lisaa(koe);
    }
    
    /**
     * Palauttaa listan luokista
     * @return ArrayList luokista
     */
    public ArrayList<Luokka> getLuokat(){
        return luokat.getLuokat();
    }

    /**
     * @param oppilas
     */
    public void poista(Oppilas oppilas) {
        oppilaat.poista(oppilas);
    }

    /**
     * Tallentaa tietokannat
     * @throws IOException
     */
    public void tallenna() throws IOException {
        luokat.tallenna();        
        oppilaat.tallenna();
        arvosanat.tallenna();
        kokeet.tallenna();
    }
    
    /**
     * Tallennetaan oppilaat
     */
    public void tallennaOppilaat(){
        try {
            oppilaat.tallenna();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tallennetaan arvosanat
     */
    public void tallennaArvosanat(){
        try {
            arvosanat.tallenna();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tallennetaan luokat
     */
    public void tallennaLuokat(){
        try {
            luokat.tallenna();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tallennetaan kokeet
     */
    public void tallennaKokeet(){
        try {
            kokeet.tallenna();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Pyydet��n merkkijonoa vastaavia Luokka-olio lista ja l�hetet��n se eteenp�in
     * @param text merkkijono
     * @return lista merkkijonon sis�lt�vist� Luokka-olioista
     */
    public ArrayList<Luokka> haeMerkkijonollaLuokat(String text) {
        return luokat.haeMerkkijonollaLuokat(text);
    }
    


}

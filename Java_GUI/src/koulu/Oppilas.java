package koulu;

import java.io.PrintStream;

/**
 * Yhden oppilaan tiedot.
 * pit�� yll� varsinaista rekisteri� oppilaista 
 * lukee ja kirjoittaa oppilaidenNimet.dat fileen
 * osaa etsi� ja lajitella 
 * @author vmoja
 * @versio 3.7.2014
 *
 */
public class Oppilas implements Comparable<Oppilas> {
    private String nimi;
    private int luokanId;
    private int id;
    private static int seuraavaNumero = 1;
    
    /**
     * Luo oppilaan
     * @param nimi
     * @param luokka
     */
    public Oppilas(String nimi, int luokka){
        this.nimi = nimi;
        this.luokanId = luokka;
        this.id = seuraavaNumero;
        seuraavaNumero++;
    }
    
    /**
     * Kuormitetaan konstruktoria, ja t�t� k�ytet��n, kun luetaan tiedostosta aluksi
     * @param nimi
     * @param oppilaanId
     * @param luokka
     */
    public Oppilas(String nimi, int oppilaanId, int luokka){
        this.nimi = nimi;
        this.luokanId = luokka;
        this.id = oppilaanId;
        seuraavaNumero = (oppilaanId+1);
    }
    
    /**
     * Palauttaa oppilaan id:n
     * @return oppilaan id
     * @example
     * <pre name="test">
     * Oppilas uusi = new Oppilas("Mogilny", 1);
     * uusi.getId() === 1;
     * Oppilas toinen = new Oppilas("Fedorov", 119, 2);
     * toinen.getId() === 119;
     * Oppilas vielaUusi = new Oppilas("Irbe", 2);
     * vielaUusi.getId() === 120;
     * vielaUusi.getNimi() === "Irbe";
     * </pre>
     */
    public int getId() {
        return id;
    }
    
    /**
     * Palauttaa oppilaan nimen
     * @return oppilaan nimi
     */
    public String getNimi(){
        return nimi;
    }
    
    /**
     * Palauttaa oppilaan luokan id-numeron
     * @return luokan id-numero
     */
    public int getLuokanId(){
        return luokanId;
    }

    /**
     * Tulostetaan oppilas
     * @param out
     */
    public void tulosta(PrintStream out) {
        out.println("Nimi: " + nimi + ", luokka: " + luokanId + ",  id: " + id);
    }
    
    public String toString(){
        return nimi;
        
    }

    @Override
    public int compareTo(Oppilas oppilas) {
        return this.getNimi().compareTo(oppilas.getNimi());
    }
}

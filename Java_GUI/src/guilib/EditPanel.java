package guilib;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.JTextField;

/**
 * Luokka jolla otsikko ja edit-kent�n yhdistet��n
 * @author vmoja
 * @versio 25.6.2014
 *
 */
public class EditPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    private final JLabel lblNimi = new JLabel("Nimi");
    private final JLabel labelfill1 = new JLabel(" ");
    private final JTextField txtEdit = new JTextField();
    private final JLabel labelfill2 = new JLabel(" ");

    /**
     * Create the panel.
     */
    public EditPanel() {
        txtEdit.setColumns(10);
        setAlignmentX(Component.RIGHT_ALIGNMENT);
        setPreferredSize(new Dimension(100, 20));
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        
        add(lblNimi);
        
        add(labelfill1);
        
        add(txtEdit);
        
        add(labelfill2);

    }

    public String getCaption() {
        return lblNimi.getText();
    }
    public void setCaption(String text) {
        lblNimi.setText(text);
    }
    public String getText() {
        return txtEdit.getText();
    }
    public void setText(String text_1) {
        txtEdit.setText(text_1);
    }
    public int getColumns() {
        return txtEdit.getColumns();
    }
    public void setColumns(int columns) {
        txtEdit.setColumns(columns);
    }
    
    /**
     * Asetetaan labelin leveys
     * @param w asetettava leveys
     */
    public void setLabelWidth(int w) {
        int h = getPreferredSize().height;
        lblNimi.setPreferredSize(new Dimension(w, h));    
    }
}

This repository contains some of my projects. They are a collection from different courses and/or my personal DIY-projects. All code presented here is fully written by me starting from an empty file.

**More to come:**

- Currently I'm learning Java Web (Spring, Maven), so there will be some kind of a project related to that.

- I'm also building a IoT-system called SAURON (System For Autonomous Usercount Rendering Over Network), for tracking number of people inside a selected building. So stay tuned for some new content relating Raspberry, Arduino, XBee and/or Python.